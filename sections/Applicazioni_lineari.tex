% APPLICAZIONI LINEARI

\begin{definition}[spazio delle funzioni]
	Sia $ A $ un insieme e $ V $ un $ \K $-spazio vettoriale. L'insieme $ V^A = \mathscr{F}{(A, V)} = \{f \colon A \to V\} $ con le operazioni di
	\begin{itemize}
		\item \emph{somma} $ \forall f, g \in \mathscr{F}(A, V) $: $ \forall x \in A, (f + g)(x) = f(x) + g(x) $;
		\item \emph{prodotto per scalare} $ \forall f \in \mathscr{F}(A, V), \forall \alpha \in \K $: $ \forall x \in A, (\alpha \cdot f)(x) = \alpha \cdot f(x) $
	\end{itemize}
	è lo spazio vettoriale delle funzioni da $ A $ in $ V $.
\end{definition}

\begin{definition}[applicazione lineare]
	Siano $ V $ e $ W $ due $ \K $-spazi vettoriali. Diremo che una funzione $ L \colon V \to W  $ è un'applicazione lineare (o mappa lineare o omomorfismo) se $ L $ soddisfa le seguenti proprietà
	\begin{enumerate}[label = (\roman*)]
		\item $ \forall v_1, v_2 \in V $ vale $ L(v_1 + v_2) = L(v_1) + L(v_2) $
		\item $ \forall v \in V, \forall \lambda \in \K $ vale $ L(\lambda v) = \lambda L(v)$
	\end{enumerate}
	Diretta conseguenza è la seguente proprietà chiave delle applicazioni lineari
	\begin{enumerate}[resume, label = (\roman*)]
		\item $ L(O_V) = O_W $.
	\end{enumerate}
	L'insieme delle applicazioni lineari $ \mathscr{L}{(V, W)} = \{L \colon V \to W : L \text{ è lineare}\} $ è un sottospazio vettoriale di $ \mathscr{F}{(V, W)} $
\end{definition}

\begin{definition}[nucleo]
	Siano $ V $ e $ W $ due spazi vettoriali su un campo $ \K $ e sia $ L \colon V \to W  $ un'applicazione lineare. Definiamo nucleo o kernel di $ L $, indicato con $ \ker{L}$, l'insieme degli elementi di $ V $ la cui immagine attraverso $ L $ è lo zero di $ W $. Formalmente \[\ker{L} = \{v \in V \colon L(v) = O_W\}.\]
\end{definition}

\begin{thm}
	Sia $ L \in \mathscr{L}{(V, W)} $. Allora $ \ker{L} $ è un sottospazio vettoriale di $ V $.
\end{thm}

\begin{thm}
	Sia $ L \in \mathscr{L}{(V, W)} $. Allora $ \im{L} $ è un sottospazio vettoriale di $ W $.
\end{thm}

\begin{thm}
	$ L \in \mathscr{L}{(V, W)} $ è iniettiva se e solo se $ \ker{L} = \{O_V\} $.
\end{thm}

\begin{thm}[composizione di applicazioni lineari]
	Siano $ L \colon V \to W $ e $ G \colon W \to Z $ applicazioni lineari. Allora $ G \circ L \colon V \to Z $ è applicazione lineare.
\end{thm}

\begin{thm}[inversa di un'applicazione lineare]
	Sia $ L \colon V \to W $ lineare. Allora $ L^{-1} \colon W \to V $ (se esiste) è applicazione lineare.
\end{thm}

\begin{thm}
	Sia $ L \colon \K^n \to \K^n $ un'applicazione lineare tale che $ \ker{L} = \{O_V\} $. Se $ v_1, \ldots, v_n \in V $ sono vettori linearmente indipendenti, anche $ L(v_1), \ldots, L(v_n) $ sono vettori linearmente indipendenti di $ W $.
\end{thm}

\begin{thm}[delle dimensioni]
	Sia $ L \colon V \to W $  un'applicazione lineare. Allora vale \[\dim{V} = \dim{\im{L}} + \dim{\ker{L}}\]
\end{thm}
\begin{proof}
	Se $ \im{L} = O_{W} $ allora l'enunciato è banalmente verificato perché si avrebbe $ \ker{L} = V $. Supponiamo quindi $ \dim{\im{L}} \geq 1 $. \\
	Sia $ \{w_1, \ldots, w_s\} $ base di $ \im{L} $ e scegliamo $ v_1, \ldots, v_s \in V $ tali che $ L(v_1) = w_1, \ldots, L(v_n) = w_s $. Sia $ \{u_1, \ldots, u_q\} $ base di $ \ker{L} $.\\
	\begin{itemize}
		\item \emph{Generano} - Sia $ v \in V $. Poiché $ \{w_1, \ldots, w_s\} $ è base di $ \im{L} $ esistono $ x_1, \ldots, x_s \in \K $ tali che
		\[
			L(v) = x_1 w_1 + \ldots + x_s w_s = x_1 L(v_1) + \ldots + x_s L(v_s).
		\]
		Per linearità otteniamo che
		\[
			L(v - x_1 v_1 - \ldots - x_s v_s) = O_W
		\]
		ovvero $ v - x_1 v_1 - \ldots - x_s v_s \in \ker{L} $. Poiché $ \{u_1, \ldots, u_q\} $ è base del nucleo esistono $ y_1, \ldots, y_p \in \K $ tali che
		\[
			v - x_1 v_1 - \ldots - x_s v_s = y_1 u_1 + \ldots + y_p u_p \quad \Rightarrow \quad v = x_1 v_1 + \ldots + x_s v_s + y_1 u_1 + \ldots + y_p u_p.
		\]
		\item \emph{Linearmente indipendenti} - Supponiamo esistano $ a_1 \ldots, a_s, b_1, \ldots, b_p \in \K $ tali che
		\[
			a_1 v_1 + \ldots + a_s v_s + b_1 u_1 + \ldots + b_p u_p = O_{V}.
		\]
		 Applicando $ L $ otteniamo per linearità
		 \[
		 	a_1 L(v_1) + \ldots a_s L(v_s) + O_{W} = O_{W} \Rightarrow a_1 w_1 + \ldots + a_s w_s = O_{W}.
		 \]
		 Poiché $ \{w_1, \ldots, w_s\} $ è base di $ \im{L} $ segue che $ a_1 = \ldots = a_s = 0 $. Così
		 \[
		 	b_1 u_1 + \ldots + b_p u_p = O_{V}.
		 \]
		 Poiché $ \{u_1, \ldots, u_q\} $ è base di $ \ker{L} $ segue che $ b_1 = \ldots = b_q = 0 $.
	\end{itemize}
	Dunque $ \{v_1, \ldots, v_s, u_1, \ldots, u_q\} $ è base di $ V $ dai cui segue la tesi.
\end{proof}

\begin{corollary}
	Sia $ L \colon V \to W $ un'applicazione lineare. Se $ \ker{L} = \{O_V\} $ e $ \im{L} = W $, allora $ L $ è biettiva e dunque invertibile
\end{corollary}

\begin{definition}[isomorfismo]
	$ L \in \mathscr{L}{(V, W)} $ biettiva si dice isomorfismo. Se tale applicazioni esiste si dice che $ V $ e $ W $ sono isomorfi e in particolare risulta $ \dim{V} = \dim{W} $.
\end{definition}

\begin{definition}[endomorfismo]
	$ L \in \mathscr{L}{(V, V)} $ dallo spazio in sé si dice endomorfismo. L'insieme degli endomorfismi viene indicato con $ \End{(V)} $ ed è un sottospazio vettoriale di $ \mathscr{F}{(V, V)} $
\end{definition}

\begin{thm}[decomposizione di Fitting] $\dagger$
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $ e $ f \in \End{(V)} $. Allora esiste un intero $ k \leq n $ tale che \begin{enumerate}[label = (\roman*)]
		\item $ \ker{f^{k}} = \ker{f^{k + 1}} $;
		\item $ \im{f^{k}} = \im{f^{k + 1}} $;
		\item $ f\lvert_{\im{f^{k}}} \colon \im{f^{k}} \to \im{f^{k}} $ è un isomorfismo;
		\item $ f(\ker{f^{k}}) \subseteq \ker{f^{k}} $;
		\item $ f\lvert_{\ker{f^{k}}} \colon \ker{f^{k}} \to \ker{f^{k}} $ è nilpotente;
		\item $ V = \ker{f^{k}} \oplus \im{f^{k}} $.
	\end{enumerate}
\end{thm}

\begin{prop} \footnote{Primo compitino 2017/2018}
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $ e $ f \in \End{(V)} $. Allora:
	\begin{enumerate}
		\item $ \forall j \in \N, \ \ker{f^{j}} \subseteq \ker{f^{j + 1}} $;
		\item se esiste $ j \in \N : \ker{f^{j}} = \ker{f^{j + 1}} $ allora $ \forall m \geq j, \ \ker{f^{m}} = \ker{f^{m + 1}} $;
		\item se esiste $ j \in \N : f^{j} = 0 $ (endomorfismo nullo), allora $ f^{n} = 0 $.
	\end{enumerate}
\end{prop}

\begin{prop} $\dagger$
	Siano $ V, W, Z $ tre $ \K $-spazi vettoriali e $ L \colon V \to W $ e $ G \colon W \to Z $ applicazioni lineari. Allora \[\dim{\ker{(G \circ L)}} = \dim{\ker{L}} + \dim{(\ker{G} \cap \im{L})} \leq \dim{\ker{L}} + \dim{\ker{G}}\]
\end{prop}
\begin{proof}
	Dal teorema delle dimensioni sappiamo che \[ \dim{V} = \dim{\im{L}} + \dim{\ker{L}} = \dim{\im{(G \circ L)}} + \dim{\ker{(G \circ L)}} \] da cui otteniamo che $ \dim{\ker{(G \circ L)}} = \dim{\ker{L}} + \dim{\im{L}} - \dim{\im{(G \circ L)}} $. Consideriamo allora la restrizione di $ G $ all'immagine di $ L $, $ G\lvert_{\im{L}} \colon \im{L} \to Z $. Applicando il teorema delle dimensioni a $ G\lvert_{\im{L}} $ otteniamo che $ \dim{\im{L}} = \dim{\ker{G\lvert_{\im{L}}}} + \dim{\im{G\lvert_{\im{L}}}} $. Ma d'altra parte $ \ker{G\lvert_{\im{L}}} = \ker{G} \cap \im{L} $ e $ \im{G\lvert_{\im{L}}} = \im{(G \circ L)} $. Otteniamo quindi $ \dim{\im{L}} = \dim{(\ker{G} \cap \im{L})} + \dim{\im{(G \circ L)}} $ che inserita nella relazione trovata in precedenza dà l'uguaglianza cercata. La disuguaglianza segue banalmente dal fatto che $ (\ker{G} \cap \im{L}) \subseteq \ker{G} $.
\end{proof}