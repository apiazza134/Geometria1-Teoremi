% APPLICAZIONI LINEARI E MATRICI

\begin{definition}[rappresentazione di un vettore in una base]
	Sia $ V $ un $ \K $-spazio vettoriale di $ \dim{V} = n $ e sia $ \mathscr{B} = \{e_1, \ldots, e_n\} $ una base di $ V $. Un $ v \in V $ si scrive in modo unico come $ v = x_1 e_1 + \ldots + x_n e_n $. L'applicazione
	\begin{align*}
		[\, \cdot \,]_{\mathscr{B}} \colon V & \to \K^n \\
		v & \mapsto (x_1, \ldots, x_n)
	\end{align*}
	è quindi un isomorfismo. Definiamo quindi la scrittura di $ v $ in base $ \mathscr{B} $ come il vettore riga o vettore colonna dato dall'isomorfismo: \[[v]_{\mathscr{B}} = (x_1, \ldots, x_n) =
	\begin{pmatrix}
	x_1 \\
	\vdots \\
	x_n
	\end{pmatrix}.\]
\end{definition}

\begin{definition}[prodotto scalare standard su $ \K^n $]
	Dati $ a = (a_1, \ldots, a_n), b = (b_1, \ldots, b_n) \in \K^n $ definiamo prodotto scalare standard tra $ a $ e $ b $ come
	\[a \cdot b =
	\begin{pmatrix}
	a_1 \\
	\vdots \\
	a_n
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
	b_1 \\
	\vdots \\
	b_n
	\end{pmatrix}
	= [a]_{\mathscr{C}}^{t} [b]_{\mathscr{C}} = a_1 b_1 + \ldots + a_n b_n.\]

\end{definition}

\begin{thm}
	Sia $ L \colon \K^n \to \K $ un'applicazione lineare. Allora esiste un unico vettore $ A \in \K^n $ tale che per ogni $ X \in \K^n $ \[L(X) = A \cdot X\]
\end{thm}

\begin{thm}
	Esiste una corrispondenza biunivoca tra \[\mathscr{L}{(\K^n, \K^m)} \leftrightarrow \mathrm{Mat}_{m \times n}(\K)\]
	\begin{enumerate}
		\item Data una matrice $ M \in \mathrm{Mat}_{m \times n}(\K) $ è possibile associare ad essa un'applicazione lineare
		\begin{align*}
		L \colon \K^n & \to \K^m \\
		X & \mapsto MX
		\end{align*}
		\item Sia $ L \colon \K^n \to \K^m $ un'applicazione lineare. Allora esiste una matrice $ M \in \mathrm{Mat}_{m \times n}(\K) $ tale che $ \forall X \in \K^n $, $ L(X) = MX $. Se $ \mathscr{C} = \{e_1, \ldots , e_n\} $ è la base canonica di $ \K^n $, le colonne di $ M $ sono $ L(e_1), \ldots, L(e_n) $.
	\end{enumerate}
\end{thm}

\begin{thm}
	Siano $ V $ e $ W $ spazi vettoriali su un campo $ \K $ e sia $ L \colon \K^n \to \K^m $ un'applicazione lineare. Siano inoltre $ \mathscr{B} = \{v_1, \ldots, v_n\} $ e $ \mathscr{B}' = \{w_1, \ldots, w_m\} $ basi rispettivamente di $ V $ e di $ W $. Preso $ v \in V $ esiste una matrice $ M \in \mathrm{Mat}_{m \times n}(\K) $ tale che \[[L(v)]_{\mathscr{B}'} = M [v]_{\mathscr{B}}.\]
\end{thm}

\begin{thm}
	Siano $ V $ e $ W $ due spazi vettoriali su $ \K $ di dimensione $ n $ e $ m $. Siano $ \mathscr{B} = \{v_1, \ldots, v_n\} $ e $ \mathscr{B}' = \{w_1, \ldots, w_m\} $ basi rispettivamente di $ V $ e di $ W $. \\ Indicando con $ \mathscr{L}(V, W) $ l'insieme delle applicazioni lineari da $ V $ a $ W $ e con $ \left [L \right ]_{\mathscr{B}'} ^{\mathscr{B}} $ la matrice associata a $ L $ rispetto alle basi $ \mathscr{B} $ e $ \mathscr{B}' $, si ha che
	\begin{align*}
	M \colon \mathscr{L}(V, W) & \to \mathrm{Mat}_{m \times n}(\K)\\
	L & \mapsto \left [L \right ]_{\mathscr{B}'} ^{\mathscr{B}}
	\end{align*}
	è un'applicazione lineare ed è un isomorfismo tra lo spazio delle applicazioni lineari e lo spazio delle matrici.
\end{thm}

\begin{thm}[matrice di funizione composta]
	Siano $ V $, $ W $ e $ U $ spazi vettoriali e siano
    \[
        \mathscr{B} = \{v_1, \ldots, v_n\} \qquad \mathscr{B}' = \{w_1, \ldots, w_m\} \qquad \mathscr{B}'' = \{u_1, \ldots, u_s\}
    \]
    basi di $ V $, $ W $ e $ U $ rispettivamente. Siano inoltre $ F \colon V \to W $ e $ G \colon W \to U $ lineari. Allora \[ \left [G \circ F \right ]_{\mathscr{B}''} ^{\mathscr{B}} = \left [G \right ]_{\mathscr{B}''} ^{\mathscr{B}'} \left [F \right ]_{\mathscr{B}'} ^{\mathscr{B}} \]
\end{thm}

\begin{thm}
	Sia $ L \colon V \to W $ un'applicazione lineare e $ B \colon V \to V $ lineare e invertibile. Allora vale che \[\im{L \circ B} = \im{L} \quad \mathrm{e} \quad \dim{\ker{L \circ B}} = \dim{\ker{L}}\] In altre parole se $ \left [L \right ] $ è la matrice associata a $ L $ e $ \left [B \right ] $ è la matrice invertibile delle mosse di colonna associata a $ B $, la matrice $ \left [L \right ] \left [B \right ] $ è una matrice ridotta a scalini per colonna in cui lo $ Span $ delle colonne è lo stesso dello span delle colonne di $ \left [L \right ] $. Più brevemente la riduzione di Gauss per colonne lascia invariato lo $ Span $ delle colonne.
\end{thm}

\begin{thm}
	Sia $ L \colon V \to W $ un'applicazione lineare e $ U \colon W \to W $ lineare e invertibile. Allora vale che \[\ker{U \circ L} = \ker{L} \quad \mathrm{e} \quad \dim{\im{U \circ L}} = \dim{\im{L}}\] In altre parole se $ \left [L \right ] $ è la matrice associata a $ L $ e $ \left [U \right ] $ è la matrice invertibile delle mosse di riga associata a $ U $, la matrice $ \left [U \right ] \left [L \right ] $ è una matrice ridotta a scalini per riga che ha lo stesso $ \ker $ di $ \left [L \right ] $. Più brevemente la riduzione di Gauss per righe lascia invariato lo spazio delle soluzioni di un sistema lineare omogeneo.
\end{thm}

\begin{definition}[rango]
	Sia $ A \in \mathrm{Mat}_{m \times n} (\K) $ definiamo rango di A, $ \rg{A} $, in modo equivalente come
	\begin{enumerate}
		\item il numero massimo di colonne linearmente indipendenti (numero di \emph{pivot} colonna di $ A $ ridotta a scalini per colonna)
		\item il numero massimo di righe linearmente indipendenti (numero di \emph{pivot} riga di $ A $ ridotta a scalini per righe)
		\item la $ \dim{\im{L}} $, dove $ L \colon \K^n \to \K^m $ è l'applicazione lineare associata $ \left [L \right ]_{\mathscr{B}'} ^{\mathscr{B}} = A $
	\end{enumerate}
\end{definition}
