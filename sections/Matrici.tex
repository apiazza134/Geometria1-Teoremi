% MATRICI

\begin{definition}[matrice]
	Una matrice $ m \times n $ a coefficienti in $ \K $ è un tabella ordinata di $ m $ righe e $ n $ colonne i cui elementi, dette entrate, appartengono ad un campo $ \K $. L'insieme delle matrici $ m \times n $ a coefficienti nel campo $ \K $ viene indicato con $ \mathrm{Mat}_{m \times n}{(\K)} $. \\ Dati $ a_{ij} \in \K $ con $ i = 1, \ldots, m $ e $ j = 1, \ldots, n $ diremo che $ A \in \mathrm{Mat}_{m \times n} (\K) $ e scriveremo
	\[ A = (a_{ij}) =
	\begin{pmatrix}
		a_{11} & \cdots  & a_{1n} \\
		\vdots & \ddots & \vdots \\
		a_{m1} & \cdots  & a_{mn} \\
	\end{pmatrix}
	=
	\begin{pmatrix}
	A_{1} \\
	\vdots \\
	A_{n}
	\end{pmatrix}
	=
	\begin{pmatrix}
	A^{1} & \cdots & A^{m}
	\end{pmatrix}
	\]
	dove $ A_i $ rappresenta la $ i $-esima riga della matrice e $ A^{j} $ rappresenta la $ j $-esima colonna della matrice.\\
	Una matrice si dice quadrata se $ n = m $. 
\end{definition}

\begin{definition}
	Per ogni $ A, B \in \mathrm{Mat}_{m \times n}{(\K)} $ definiamo
	\begin{enumerate}
		\item \emph{somm tra matricia}: $ A + B = (a_{ij} + b_{ij}) $;
		\item \emph{prodotto per scalare}: $ \lambda A = (\lambda a_{ij}) $;
	\end{enumerate}
	L'insieme $ \mathrm{Mat}_{m \times n}{(\K)} $ con le operazioni definite sopra è uno spazio vettoriale. 
\end{definition}

\begin{prop} $\dagger$
	L'applicazione 
	\begin{align*}
		f \colon & \K^n \to \mathrm{Mat}_{n \times 1}{(\K)}\\
		& (x_1, \ldots, x_n) \mapsto \begin{pmatrix} x_1 \\ \vdots \\ x_n \end{pmatrix}
	\end{align*}
è biettiva e quindi scriveremo un vettore di $ \K^n $ come $ n $-upla (\emph{vettore riga}) o come \emph{vettore colonna}
\end{prop}

\begin{definition}[prodotto tra matrici]
	Siano $ A \in \mathrm{Mat}_{m \times n}(\K) $ e $ B \in \mathrm{Mat}_{n \times s} (\K) $. Definiamo il prodotto tra le due matrici $ A B $ come la matrice $ m \times s $ che ha come coefficiente di posto $ ij $ il prodotto scalare tra l'$ i $-esimo vettore riga di $ A $ e il $ j $-esimo vettore colonna di $ B $
	\[
		AB = (A_{i} \cdot B^{j}) = 
		\begin{pmatrix}
		A_1 \cdot B^1 & A_1 \cdot B^2 & \cdots & A_1 \cdot B^s \\
		A_2 \cdot B^1 & A_2 \cdot B^2 & \cdots & A_2 \cdot B^s \\
		\vdots & \vdots & \ddots & \vdots \\
		A_n \cdot B^1 & A_n \cdot B^2 & \cdots & A_n \cdot B^s 
		\end{pmatrix}
		= \left(\sum_{k = 1}^{n}a_{ik} b_{kj}\right)
	\]
\end{definition}

\begin{definition}[prodotto tra matrici quadrate]
	Siano $ A, B,  \in \mathrm{Mat}_{n \times n}(\K) $ matrici quadrate. Allora 
	\begin{enumerate}[label = (\roman*)]
		\item $ A (B C) = (A B) C = A B C $;
		\item Esiste l'elemento neutro del prodotto detta matrice \emph{identità} o unità 
		\[\Id = \Id_{n} =
		\begin{pmatrix}
		1 & \cdots & 0 \\
		\vdots & \ddots & \vdots \\
		0 & \cdots & 1
		\end{pmatrix}\]
		tale che $ A \Id = \Id A = A $;
		\item $ (A + B) C = A C + B C $ e $ C (A + B) = C A + C B $;
		\item $ \forall \lambda, \mu \in \K, \ (\lambda A) (\mu B) = (\lambda \mu) AB $.
	\end{enumerate}
	Le proprietà (i), (ii) e (iii) conferiscono a $ \mathrm{Mat}_{n \times n}(\K) $ dotato della somma e del prodotto tra matrici la struttura di anello (\emph{Nota}: in generale il prodotto tra matrici non è commutativo).
\end{definition}

\begin{definition}[matrice invertibile]
	Una matrice $ A \in \mathrm{Mat}_{n \times n}(\K) $ si dice invertibile se esiste $ B \in \mathrm{Mat}_{n \times n}(\K) $ tale che $ A B = B A = \Id $. Tale $ B $ è detta matrice inversa e indicata con $ A^{-1} $. \\
	Il sottoinsieme delle matrici $ \mathrm{Mat}_{n \times n}(\K) $ invertibili dotato del prodotto tra matrici è un gruppo e se è dotato anche della somma tra matrici è un corpo.  
\end{definition}

\begin{definition}[matrice trasposta]
	Sia $ A = (a_{ij}) \in \mathrm{Mat}_{m \times n}(\K) $. Definiamo matrice trasposta la matrice $ A^{t} = (a_{ij})^t = (a_{ji}) $. 
\end{definition}

\begin{propriety}[della trasposta]
	\textsf{$ (AB)^t = B^t \, A^t $ e linearità $ (\lambda A + \mu B)^t = \lambda A^t + \mu B^t $}
\end{propriety}

\begin{definition}[matrici coniugate]
	Due matrici $ A $ e $ B $ si dicono coniugate se esiste una matrice $ P $ invertibile tale che \[B = P^{-1} A P.\] \emph{Matrici coniugate rappresentano la stessa applicazioni lineari viste in due basi diverse.}
\end{definition}

\begin{definition}[traccia]
	Sia $ M $ una matrice quadrata $ n \times n $. La traccia di $ M $ è la somma degli elementi sulla diagonale 
	\[\tr{(M)} = \tr{ 
	\begin{pmatrix}
	a_{11} & \cdots  & a_{1n} \\
	\vdots & \ddots & \vdots \\
	a_{n1} & \cdots  & a_{nn} \\
	\end{pmatrix}}
	= a_{11} + \ldots + a_{nn}\]
\end{definition}

\begin{propriety}[della traccia]
	La traccia gode delle seguenti proprietà
	\begin{enumerate}[label=(\roman*)]
		\item $ \tr (A + B) = \tr(A) + \tr(B) $ e $ \tr(\lambda A) = \lambda \, \tr (A) $
		\item $ \tr(\prescript{t}{}{A}) = \tr (A) $
		\item $ \tr (AB) = \tr (BA) = \sum_{i = 1}^{n} \sum_{j = 1}^{n} a_{ij} b_{ji} $
	\end{enumerate}
\end{propriety}

\begin{thm}[invarianza della traccia per coniugio]
	Se $ A $ e $ B $ sono matrici coniugate, allora $ \tr (A) = \tr (B) $
\end{thm}

Roba su riduzione a scalini...

\begin{thm} $\dagger$
	Tutte e sole le matrici $ A \in \mathrm{Mat}_{n \times n}(\K) $ che commutano con ogni matrice $ B \in  \mathrm{Mat}_{n \times n}(\K) $ sono multipli dell'identità. \[AB = BA, \ \forall B \in \mathrm{Mat}_{n \times n}(\K) \quad \iff \quad \exists \lambda \in \K : A = \lambda \Id\]
\end{thm}